//
//  RengineSession.cpp
//  cpp-sfk
//
//  Created by Ilya Rezvov on 11.06.14.
//  Copyright (c) 2014 irezvov. All rights reserved.
//

#include "RengineSession.h"
#include "SFK.h"

RengineSession::RengineSession(SFK* sfk, socket_ptr sock) : Session(sock), sfk(sfk) {
    cout << "Start session" << endl;
}

void RengineSession::on_message(sfk::Msg& msg, string& data)  {
    cout << "Receive msg with type: " << msg.mtype() << " and data: " << data << endl;
    switch (msg.mtype()) {
        case sfk::MsgType::SESSION:
            start_session(msg.session());
            break;
        
        case sfk::MsgType::DATA:
            on_data(msg.data());
            break;
            
        default:
            cout << "Unimplemented message with type: " << msg.mtype() << endl;
            break;
    }
}

void RengineSession::on_close()  {
    cout << "Close socket" << endl;
    sfk->unregister_session(channel);
}

void RengineSession::start_session(sfk::Session session) {
    Json::Value params;
    Json::Reader().parse(session.params(), params);
    string user_params_key(params.get("nptv-user-params-key", "user-params").asString());
    channel = params["channel"].asString();
    user_params = params[user_params_key];
    params.removeMember(user_params_key);
//    cout << "Start session with user params: " << user_params << endl;
//    cout << "With params: " << params << endl;
    auto addr = get_address();
    description = {
        channel,
        static_cast<uint32_t>(user_params["width"].asInt()),
        static_cast<uint32_t>(user_params["height"].asInt()),
        addr.first,
        addr.second,
        channel,
        0
    };
    
    send_start_merge();
    start_js_session();
    sfk->register_session(channel, RengineSession::ptr(this));
}

void RengineSession::start_js_session() {
//    Isolate* isolate = Isolate::New();
//    Isolate::Scope isolate_scope(isolate);
//    HandleScope handle_scope(isolate);
//    Handle<Context> context = Context::New(isolate);
}

void RengineSession::on_data(sfk::Data data) {
    if(data.content_type() == "custom_js_response") {
        Json::Value msg;
        msg["type"] = "custom_js_response";
        Json::Value payload;
        Json::Reader().parse(data.payload(), payload);
        msg["data"] = payload;
        sfk->send_to_webface(channel, msg);
    }
    if(data.content_type() == "js_console") {
        Json::Value msg;
        msg["type"] = "js_console";
        msg["uuid"] = channel;
        Json::Value payload;
        Json::Reader().parse(data.payload(), payload);
        msg["data"] = payload;
        sfk->send_to_webface(channel, msg);
    }
    else {
        cout << "Data message with unimplemented content type: " << data.content_type() << endl;
    }
}

void RengineSession::send_start_merge() {
    Json::Value start_merge(Json::arrayValue);
    dom.set_width(description.client_width);
    dom.set_height(description.client_height);
    dom.commit_changes();
    start_merge.append(dom.get_description());
    string merge_json(Json::FastWriter().write(start_merge));
    
    sfk::Msg msg;
    auto start = new sfk::Start();
    auto merge = new sfk::Merge();
    merge->set_length(static_cast<google::protobuf::uint32>(merge_json.length()));
    start->set_clock("local://25/3/3");
    start->set_allocated_merge(merge);
    start->set_width(description.client_width);
    start->set_height(description.client_height);
    msg.set_allocated_start(start);
    msg.set_mtype(sfk::START);
    send(msg, merge_json);
}

void RengineSession::send_javascript(string &filename, string &src) {
    sfk::Javascript* js = new sfk::Javascript;
    js->set_length(static_cast<google::protobuf::uint32_t>(src.length()));
    js->set_filename(filename);
    sfk::Msg msg;
    msg.set_mtype(sfk::JAVASCRIPT);
    msg.set_allocated_javascript(js);
    send(msg, src);
}

void RengineSession::send_merge(Json::Value const& merge_json) {
    string json_str(Json::FastWriter().write(merge_json));
    sfk::Merge* merge = new sfk::Merge;
    merge->set_length(static_cast<google::protobuf::uint32_t>(json_str.length()));
    sfk::Msg msg;
    msg.set_mtype(sfk::MsgType::MERGE);
    msg.set_allocated_merge(merge);
    send(msg, json_str);
}

void RengineSession::update_dom() {
    Json::Value changes = dom.get_dom_changes();
    if (changes.size() > 0) {
        send_merge(changes);
    }
}

string const& RengineSession::get_channel() const {
    return channel;
}

RengineSession::Description const& RengineSession::get_description() const {
    return description;
}

Root& RengineSession::get_dom() {
    return dom;
}

Json::Value RengineSession::Description::as_json() const {
    Json::Value data;
    data["channel"] = channel;
    data["session_id"] = session_id;
    data["client_height"] = client_height;
    data["client_width"] = client_width;
    data["rengine_host"] = rengine_host;
    data["rengine_port"] = rengine_port;
    data["start_time"] = start_time;
    return move(data);
}
