//
//  Container.h
//  cpp-sfk
//
//  Created by Ilya Rezvov on 19.06.14.
//  Copyright (c) 2014 irezvov. All rights reserved.
//

#ifndef __cpp_sfk__Container__
#define __cpp_sfk__Container__

#include "common.h"
#include "Actor.h"
#include <vector>

class Container: public Actor {
public:
    Container(string const& type, string const& id);
    Json::Value get_description() const;
    void collect_changes(vector<Json::Value> &changes);
    bool is_container() const;
    
    void add_child(Actor::ptr child);
private:
    vector<Actor::ptr> children;
};

#endif /* defined(__cpp_sfk__Container__) */
