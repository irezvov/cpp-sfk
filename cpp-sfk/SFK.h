#ifndef __cpp_sfk__SFK__
#define __cpp_sfk__SFK__

#include "common.h"
#include "RengineListener.h"
#include "RengineSession.h"
#include "WebfaceServer.h"
#include <map>

struct SfkConfig {
    string rengine_host;
    uint16_t rengine_port;
    uint16_t webface_port;
};

static SfkConfig default_config { string("10.40.6.149"), 8086, 8085 };

class SFK {
public:
    SFK(const SfkConfig cfg);
    ~SFK();
    
    void run();
    void register_session(string const & channel, RengineSession::ptr session);
    void unregister_session(string const & channel);
    RengineSession::ptr get_session(string& channel);
    map<string, RengineSession::ptr> const& get_sessions() const;
    bool session_exists(string const & channel) const;
    pair<string, uint16_t> get_listener_addr();
    
    void send_to_webface(string const& channel, string const& payload);
    void send_to_webface(string const& channel, Json::Value& payload);
private:
    const SfkConfig config;
    RengineListener listener;
    map<string, RengineSession::ptr> sessions;
    WebfaceServer webface_server;
};

#endif /* defined(__cpp_sfk__SFK__) */
