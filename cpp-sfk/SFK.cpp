#include "SFK.h"
#include <thread>

#include "RengineSession.h"
#include "dom/Rectangle.h"
#include "dom/Text.h"

SFK::SFK(const SfkConfig cfg) : config(cfg), listener(move(unique_ptr<RengineSessionFactory>(new RengineSessionFactory(this))), cfg.rengine_port),
    webface_server(this, cfg.webface_port) {
}

SFK::~SFK() {
}

void SFK::run() {
    cout << "Run SFK" << endl;
    thread([this]() {
        cout << "Start websocket server for WebFace" << endl;
        webface_server.run();
    }).detach();
    listener.start();
}

// TODO: remove it. only for test
void send_white_rect(RengineSession::ptr session) {
    thread([=]() {
        auto rect = Rectangle::create("rect");
        rect->set_color("white");
        rect->set_x(100);
        rect->set_y(100);
        rect->set_width(400);
        rect->set_height(400);
        session->get_dom().add_child(rect);
        auto text = Text::create("text");
        text->set_width(1000);
        text->set_height(50);
        text->set_color("white");
        text->set_text("hello from cpp-sfk");
        session->get_dom().add_child(text);
        session->update_dom();
        for (int i = 1; i < 10; i++) {
            sleep(1);
            rect->set_x(100 + 10*i);
            rect->set_y(100 + 5*i);
            session->update_dom();
        }
    }).detach();
}

void SFK::register_session(string const &channel, RengineSession::ptr session) {
    cout << "Register session with channel: " << channel << endl;
    sessions[channel] = session;
    webface_server.on_new_session(session);
    send_white_rect(session);
}

void SFK::unregister_session(string const &channel) {
    if (sessions.count(channel) > 0) {
        cout << "Unregister session with channel: " << channel << endl;
        RengineSession::ptr session = sessions[channel];
        sessions.erase(channel);
        webface_server.on_remove_session(session);
    }
}

void SFK::send_to_webface(string const &channel, string const &payload) {
    webface_server.send_to_channel(channel, payload);
}

void SFK::send_to_webface(string const &channel, Json::Value &payload) {
    webface_server.send_to_channel(channel, payload);
}

RengineSession::ptr SFK::get_session(string &channel) {
    if (sessions.count(channel)) {
        return sessions[channel];
    }
    return RengineSession::ptr(nullptr);
}

map<string, RengineSession::ptr> const& SFK::get_sessions() const {
    return sessions;
}

bool SFK::session_exists(string const &channel) const {
    return sessions.count(channel) > 0;
}

pair<string, uint16_t> SFK::get_listener_addr() {
    return move(make_pair(config.rengine_host, config.rengine_port));
}