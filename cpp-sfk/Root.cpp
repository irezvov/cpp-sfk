//
//  Root.cpp
//  cpp-sfk
//
//  Created by Ilya Rezvov on 19.06.14.
//  Copyright (c) 2014 irezvov. All rights reserved.
//

#include "Root.h"

Root::Root(): Container("NptvRoot", "root"), color("black") { }

Json::Value Root::get_description() const {
    Json::Value description(Container::get_description());
    description["color"] = get_color();
    return move(description);
}

string const& Root::get_color() const {
    return color;
}

void Root::set_color(string const& color) {
    is_changed = true;
    changes["color"] = color;
    this->color = color;
}

Json::Value Root::get_dom_changes() {
    vector<Json::Value> changes;
    collect_changes(changes);
    Json::Value diff(Json::arrayValue);
    for (auto& change: changes) {
        diff.append(change);
    }
    return move(diff);
}