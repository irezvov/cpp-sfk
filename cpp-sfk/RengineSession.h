//
//  RengineSession.h
//  cpp-sfk
//
//  Created by Ilya Rezvov on 11.06.14.
//  Copyright (c) 2014 irezvov. All rights reserved.
//

#ifndef __cpp_sfk__RengineSession__
#define __cpp_sfk__RengineSession__

#include "common.h"
#include <json/json.h>

#include "RengineListener.h"
#include "Root.h"

class SFK;

class RengineSession: public Session {
public:
    typedef shared_ptr<RengineSession> ptr;
    
    struct Description {
        string channel;
        uint32_t client_width;
        uint32_t client_height;
        string rengine_host;
        uint16_t rengine_port;
        string session_id;
        uint32_t start_time;
        
        Json::Value as_json() const;
    };
    
    RengineSession(SFK* sfk, socket_ptr sock);
    
    void start_session(sfk::Session session);
    void on_data(sfk::Data data);
    void send_javascript(string& filename, string& src);
    void send_merge(Json::Value const& merge);
    void update_dom();
    
    string const& get_channel() const;
    Description const& get_description() const;
    Root& get_dom();
protected:
    void send_start_merge();
    void on_message(sfk::Msg& msg, string& data);
    void on_close();
    
    void start_js_session();
private:
    SFK* sfk;
    string channel;
    Description description;
    Json::Value user_params;
    Root dom;
};

class RengineSessionFactory: public SessionFactory {
    SFK* sfk;
public:
    RengineSessionFactory(SFK* sfk): sfk(sfk) {}
    session_ptr create(socket_ptr socket) {
        return shared_ptr<RengineSession>(new RengineSession(sfk, socket));
    }
};

#endif /* defined(__cpp_sfk__RengineSession__) */
