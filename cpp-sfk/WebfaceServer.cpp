//
//  WebfaceServer.cpp
//  cpp-sfk
//
//  Created by Ilya Rezvov on 16.06.14.
//  Copyright (c) 2014 irezvov. All rights reserved.
//

#include "WebfaceServer.h"
#include "SFK.h"

#include <boost/bind/placeholders.hpp>

WebfaceServer::WebfaceServer(SFK* sfk, int port): port(port), sfk(sfk) {
    websocket.clear_access_channels(websocketpp::log::alevel::all);
    
    websocket.set_open_handler(boost::bind(&WebfaceServer::on_open, this, _1));
    websocket.set_message_handler(boost::bind(&WebfaceServer::on_message, this, _1, _2));
    websocket.set_close_handler(boost::bind(&WebfaceServer::on_close, this, _1));
}

void WebfaceServer::run() {
    websocket.init_asio();
    websocket.listen(port);
    websocket.start_accept();
    websocket.run();
}

void WebfaceServer::send_to_channel(string const &channel, string const &data) {
    if (attributes.count(channel)) {
        auto conn = attributes[channel].connection;
        websocket.send(conn, data, websocketpp::frame::opcode::text);
    }
}

void WebfaceServer::send_to_channel(string const &channel, Json::Value &data) {
    string data_str = Json::FastWriter().write(data);;
    send_to_channel(channel, data_str);
}

void WebfaceServer::send_json(websocketpp::connection_hdl conn, Json::Value &data) {
    string data_str = Json::FastWriter().write(data);;
    websocket.send(conn, data_str, websocketpp::frame::opcode::text);
}

void WebfaceServer::send_to_all_with_uuid(Json::Value &data) {
    for (auto conn: open_connections) {
        Json::Value own_data(data);
        if (connections.count(conn) > 0) {
            own_data["uuid"] = connections[conn];
        }
        else {
            own_data["uuid"] = "undefined";
        }
        string data_str = Json::FastWriter().write(own_data);
        websocket.send(conn, data_str, websocketpp::frame::opcode::text);
    }
}

void WebfaceServer::send_to_all(string &data) {
    for (auto conn: open_connections) {
        websocket.send(conn, data, websocketpp::frame::opcode::text);
    }
}

void WebfaceServer::send_to_all(Json::Value &data) {
    string data_str = Json::FastWriter().write(data);;
    send_to_all(data_str);
}

void WebfaceServer::on_new_session(RengineSession::ptr session) {
    string const& channel = session->get_channel();
    if (attributes.count(channel)) {
        WebfaceData& data = attributes[channel];
        if (data.prepushes.size() > 0) {
            cout << "Send prepushes for session " << channel << endl;
            for(auto it: data.prepushes) {
                string filename(it.first);
                session->send_javascript(filename, it.second);
            }
        }
    }
    notify_about_new_session(session);
}

void WebfaceServer::on_remove_session(RengineSession::ptr session) {
    string const& channel = session->get_channel();
    attributes.erase(channel);
    notify_about_close_session(session);
}

void WebfaceServer::on_open(websocketpp::connection_hdl hdl) {
    open_connections.insert(hdl);
}

void WebfaceServer::on_message(websocketpp::connection_hdl hdl, server::message_ptr msg) {
    Json::Value req;
    if (Json::Reader().parse(msg->get_payload(), req)) {
        string msg_type(req.get("type", "get_message").asString());
        if (msg_type == "get_connection") {
            on_get_connection(hdl, req);
        }
        else if (msg_type == "custom_js") {
            on_custom_js(req);
        }
        else if (msg_type == "session_list") {
            on_sessions_list(hdl, req);
        }
        else {
            cout << "Websocket message with unknown type: " << msg_type << endl;
        }
    }
    else {
        cout << "Message is bad JSON: " << msg->get_payload() << endl;
    }
}

void WebfaceServer::on_close(websocketpp::connection_hdl hdl) {
    if (connections.count(hdl)) {
        string& channel = connections[hdl];
        attributes.erase(channel);
        connections.erase(hdl);
    }
    open_connections.erase(hdl);
}

void WebfaceServer::on_get_connection(websocketpp::connection_hdl hdl, Json::Value &req) {
    // store session and prepushes
    string channel = req["uuid"].asString();
    if (sfk->session_exists(channel)) {
        websocket.close(hdl, websocketpp::close::status::normal, "connection already exists");
        return;
    }
    
    map<string, string> pushes;
    Json::Value& prepush = req["data"]["prepush"];
    for (auto key: prepush.getMemberNames()) {
        pushes[key] = prepush[key].asString();
    }
    attributes[channel] = { move(pushes), hdl };
    connections[hdl] = channel;
    
    // send response
    Json::Value res;
    res["uuid"] = req["uuid"];
    res["callback_id"] = req["callback_id"];
    Json::Value sfk_addr;
    pair<string, uint16_t> addr(sfk->get_listener_addr());
    sfk_addr["sfk_host"] = addr.first;
    sfk_addr["sfk_port"] = addr.second;
    res["data"] = sfk_addr;
    send_to_channel(channel, res);
}

void WebfaceServer::on_custom_js(Json::Value &req) {
    string channel = req["uuid"].asString();
    if (sfk->session_exists(channel)) {
        string filename = req["data"]["filename"].asString();
        string source = req["data"]["source"].asString();
        sfk->get_session(channel)->send_javascript(filename, source);
    }
}

void WebfaceServer::on_sessions_list(websocketpp::connection_hdl hdl, Json::Value &req) {
    string channel = req["uuid"].asString();
    auto sessions = sfk->get_sessions();
    Json::Value data;
    data["total_sessions_count"] = static_cast<uint32_t>(sessions.size());
    Json::Value sessions_json(Json::arrayValue);
    for (auto it: sessions) {
        sessions_json.append(it.second->get_description().as_json());
    }
    data["sessions"] = sessions_json;
    
    Json::Value res;
    res["uuid"] = channel;
    res["callback_id"] = req["callback_id"];
    res["data"] = data;
    websocket.send(hdl, Json::FastWriter().write(res), websocketpp::frame::opcode::text);
}

void WebfaceServer::notify_about_new_session(RengineSession::ptr session) {
    Json::Value msg;
    msg["type"] = "create_session";
    msg["data"] = session->get_description().as_json();
    send_to_all_with_uuid(msg);
}

void WebfaceServer::notify_about_close_session(RengineSession::ptr session) {
    Json::Value msg;
    msg["type"] = "close_session";
    msg["data"] = session->get_description().as_json();
    send_to_all_with_uuid(msg);
}
