//
//  main.cpp
//  cpp-sfk
//
//  Created by Ilya Rezvov on 07.06.14.
//  Copyright (c) 2014 irezvov. All rights reserved.
//

#include "SFK.h"

int main(int argc, const char * argv[])
{

    unique_ptr<SFK> sfk(new SFK(default_config));
    
    sfk->run();
    
    return 0;
}

