//
//  WebfaceServer.h
//  cpp-sfk
//
//  Created by Ilya Rezvov on 16.06.14.
//  Copyright (c) 2014 irezvov. All rights reserved.
//

#ifndef __cpp_sfk__WebfaceServer__
#define __cpp_sfk__WebfaceServer__

#include "common.h"
#include "RengineSession.h"
#include <map>
#include <set>

#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

#include <json/json.h>

class SFK;

typedef websocketpp::server<websocketpp::config::asio> server;

struct WebfaceData {
    map<string,string> prepushes;
    websocketpp::connection_hdl connection;
};

class WebfaceServer {
public:
    WebfaceServer(SFK* sfk, int port);
    
    void run();
    void send_to_channel(string const & channel, string const & data);
    void send_to_channel(string const & channel, Json::Value& data);
    
    void send_to_all(string& data);
    void send_to_all(Json::Value& data);
    void on_new_session(RengineSession::ptr session);
    void on_remove_session(RengineSession::ptr session);
    
    void notify_about_new_session(RengineSession::ptr session);
    void notify_about_close_session(RengineSession::ptr session);
protected:
    void on_open(websocketpp::connection_hdl hdl);
    void on_message(websocketpp::connection_hdl hdl, server::message_ptr msg);
    void on_close(websocketpp::connection_hdl hdl);
    
    void on_get_connection(websocketpp::connection_hdl hdl, Json::Value& req);
    void on_custom_js(Json::Value& req);
    void on_sessions_list(websocketpp::connection_hdl hdl, Json::Value& req);
private:
    void send_json(websocketpp::connection_hdl hdl, Json::Value& data);
    void send_to_all_with_uuid(Json::Value& data);
    
    server websocket;
    int port;
    SFK* sfk;
    map<websocketpp::connection_hdl, string> connections;
    map<string, WebfaceData> attributes;
    set<websocketpp::connection_hdl> open_connections;
};

#endif /* defined(__cpp_sfk__WebfaceServer__) */
