//
//  Container.cpp
//  cpp-sfk
//
//  Created by Ilya Rezvov on 19.06.14.
//  Copyright (c) 2014 irezvov. All rights reserved.
//

#include "Container.h"

Container::Container(string const& type, string const& id) : Actor(type, id) {
}

Json::Value Container::get_description() const {
    Json::Value description(Actor::get_description());
    Json::Value children_json(Json::arrayValue);
    for(auto child: children) {
        children_json.append(child->get_description());
    }
    description["children"] = children_json;
    return move(description);
}

void Container::collect_changes(vector<Json::Value> &changes) {
    if (has_changes()) {
        changes.push_back(get_changes());
        commit_changes();
    }
    for (auto child: children) {
        child->collect_changes(changes);
    }
}

void Container::add_child(Actor::ptr child) {
    is_changed = true;
    if (!changes.isMember("children")) {
        changes["children"] = Json::Value(Json::arrayValue);
    }
    changes["children"].append(child->get_description());
    children.push_back(child);
}
