#ifndef __cpp_sfk__RengineListener__
#define __cpp_sfk__RengineListener__

#include "common.h"
#include <boost/asio.hpp>
#include "proto/sfk.pb.h"

using namespace boost::asio;
using namespace boost::asio::ip;

typedef shared_ptr<tcp::socket> socket_ptr;

class RengineListener;

class Session {
public:
    friend RengineListener;
    
    Session(socket_ptr socket);
    void send(sfk::Msg& msg);
    void send(sfk::Msg& msg, string& data);
    
    pair<string, uint16_t> get_address();
protected:
    virtual void on_message(sfk::Msg& msg, string& data) = 0;
    virtual void on_close() { };
    socket_ptr socket;
};

typedef shared_ptr<Session> session_ptr;

class SessionFactory {
public:
    virtual session_ptr create(socket_ptr socket) = 0;
};

#pragma pack(push,1)
struct rengine_msg_header {
    uint32_t magic;
    uint32_t msg_length;
    uint32_t data_length;
};
#pragma pack(pop)

class RengineListener {
public:
    RengineListener(unique_ptr<SessionFactory> factory, int port);
    ~RengineListener();
    
    RengineListener& operator=(const RengineListener&) = delete;
    RengineListener(const RengineListener&) = delete;
    
    void start();
    void shutdown();
private:
    void start_accept();
    void accept(socket_ptr sock, boost::system::error_code ec);
    
    bool handle_read_error(session_ptr session, boost::system::error_code ec);
    
    void read_header(session_ptr session);
    void handle_header(session_ptr session, shared_ptr<char> buf, boost::system::error_code ec, size_t size);
    void handle_message(session_ptr session, shared_ptr<char> buf, pair<uint32_t, uint32_t> lengths, boost::system::error_code ec, size_t size);
    
    static rengine_msg_header parse_header(char bytes[12]);
    
    bool started;
    io_service service;
    tcp::endpoint ep;
    tcp::acceptor acceptor;
    vector<session_ptr> sessions;
    unique_ptr<SessionFactory> factory;
};

#endif /* defined(__cpp_sfk__RengineListener__) */
