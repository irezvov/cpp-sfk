#include "RengineListener.h"

#define MAGIC 0xFEED5EED

RengineListener::RengineListener(unique_ptr<SessionFactory> factory, int port)
: service(), ep(tcp::v4(), port), acceptor(service, ep), factory(move(factory)), started(false) { }

RengineListener::~RengineListener() { }

void RengineListener::start() {
    start_accept();
    cout << "Waiting for Rengine connections at " << ep.address().to_string() << ':' << ep.port() << endl;
    service.run();
    started = true;
}

void RengineListener::shutdown() {
    if (!started) {
        return;
    }
    for(auto session: sessions) {
        session->socket->close();
    }
}

void RengineListener::start_accept() {
    socket_ptr sock = make_shared<tcp::socket>(service);
    acceptor.async_accept(*sock, bind(&RengineListener::accept, this, sock, std::placeholders::_1));
}

void RengineListener::accept(socket_ptr sock, boost::system::error_code ec) {
    start_accept();
    
    if (ec) {
        cout << "Error on accept: " << ec.message();
        return;
    }
    
    session_ptr session = factory->create(sock);
    sessions.push_back(session);
    read_header(session);
}

bool RengineListener::handle_read_error(session_ptr session, boost::system::error_code ec) {
    if (ec) {
        if (ec == error::eof) {
            session->on_close();
        }
        else {
            cerr << "Error on read: " << ec.message() << endl;
        }
        return true;
    }
    return false;
}

void RengineListener::read_header(session_ptr session) {
    uint32_t header_size = sizeof(rengine_msg_header);
    shared_ptr<char> buf(new char[header_size], default_delete<char[]>());
    auto handler = bind(&RengineListener::handle_header, this, session, buf, std::placeholders::_1, std::placeholders::_2);
    async_read(*(session->socket), buffer(buf.get(), header_size), transfer_exactly(header_size), handler);
}

void RengineListener::handle_header(session_ptr session, shared_ptr<char> buf, boost::system::error_code ec, size_t size) {
    if (handle_read_error(session, ec)) return;
    
    rengine_msg_header header = parse_header(buf.get());
    
    if (header.magic == MAGIC) {
        cout << "Receive header."
        << " Msg Length: " << dec << header.msg_length
        << " Data Length: " << header.data_length
        << endl;
        uint32_t payload_size = header.msg_length + header.data_length;
        shared_ptr<char> msgbuf(new char[payload_size], default_delete<char[]>());
        auto lengths = make_pair(header.msg_length, header.data_length);
        auto handler = bind(&RengineListener::handle_message, this, session, msgbuf, lengths, std::placeholders::_1, std::placeholders::_2);
        async_read(*(session->socket), buffer(msgbuf.get(), payload_size), transfer_exactly(payload_size), handler);
    }
    else {
        cout << "Unrecognized message" << endl;
        read_header(session);
    }
}

void RengineListener::handle_message(session_ptr session, shared_ptr<char> buf, pair<uint32_t, uint32_t> lengths, boost::system::error_code ec, size_t size) {
    if (handle_read_error(session, ec)) return;
    cout << "Receive message body."
        << " Size: " << size
        << endl;
    string msg_string(buf.get(), lengths.first);
    sfk::Msg msg;
    msg.ParseFromString(msg_string);
    string data(buf.get() + lengths.first, lengths.second);
    session->on_message(msg, data);
    read_header(session);
}

rengine_msg_header RengineListener::parse_header(char *bytes) {
    rengine_msg_header* header = reinterpret_cast<rengine_msg_header*>(bytes);
    return { ntohl(header->magic), ntohl(header->msg_length), ntohl(header->data_length) };
}

Session::Session(socket_ptr socket) : socket(socket) {}

void Session::send(sfk::Msg& msg) {
    string empty;
    send(msg, empty);
}

void Session::send(sfk::Msg& msg, string& data) {
    string encoded;
    msg.SerializeToString(&encoded);
    rengine_msg_header header = { htonl(MAGIC), htonl(encoded.length()), htonl(data.length()) };
    
    cout << "Send message to Rengine. Message length: " << encoded.length() << " Data length: " << data.length() << endl;
    string header_str(reinterpret_cast<char*>(&header), sizeof(rengine_msg_header));
    socket->async_write_some(buffer(header_str + encoded + data), [this](boost::system::error_code ec, size_t size) {
        if(ec) {
            on_close();
        }
    });
}

pair<string, uint16_t> Session::get_address() {
    auto ep = socket->remote_endpoint();
    return make_pair(ep.address().to_string(), ep.port());
}
