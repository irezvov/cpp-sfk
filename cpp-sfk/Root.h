//
//  Root.h
//  cpp-sfk
//
//  Created by Ilya Rezvov on 19.06.14.
//  Copyright (c) 2014 irezvov. All rights reserved.
//

#ifndef __cpp_sfk__Root__
#define __cpp_sfk__Root__

#include "common.h"
#include "Container.h"

class Root: public Container {
public:
    Root();
    Json::Value get_description() const;
    
    string const& get_color() const;
    void set_color(string const& color);
    Json::Value get_dom_changes();
private:
    string color;
};

#endif /* defined(__cpp_sfk__Root__) */
