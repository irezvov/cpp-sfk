//
//  Actor.cpp
//  cpp-sfk
//
//  Created by Ilya Rezvov on 18.06.14.
//  Copyright (c) 2014 irezvov. All rights reserved.
//

#include "Actor.h"

Actor::Actor(string const& type, string const& id) : type(type), id {id}, box {0, 0, 0, 0}, opacity {255},
depth {0}, visible {true}, use_caching {false} {
    commit_changes();
}

Json::Value const& Actor::get_changes() const {
    return changes;
}

Json::Value Actor::get_description() const {
    Json::Value description;
    description["id"] = get_id();
    description["type"] = get_type();
    description["x"] = get_x();
    description["y"] = get_y();
    description["width"] = get_width();
    description["height"] = get_height();
    description["depth"] = depth;
    description["opacity"] = opacity;
    description["visible"] = visible;
    description["use-caching"] = use_caching;
    return move(description);
}

bool Actor::has_changes() const {
    return is_changed;
}

void Actor::commit_changes() {
    changes = Json::Value();
    changes["type"] = type;
    changes["id"] = id;
    is_changed = false;
}

string const& Actor::get_type() const {
    return type;
}

string const& Actor::get_id() const {
    return id;
}

void Actor::collect_changes(vector<Json::Value> &changes) {
    if (has_changes()) {
        changes.push_back(get_changes());
        commit_changes();
    }
}

float Actor::get_x() const {
    return box.x;
}

void Actor::set_x(float x) {
    is_changed = true;
    changes["x"] = x;
    box.x = x;
}

float Actor::get_y() const {
    return box.y;
}

void Actor::set_y(float y) {
    is_changed = true;
    changes["y"] = y;
    box.y = y;
}

float Actor::get_width() const {
    return box.width;
}

void Actor::set_width(float width) {
    is_changed = true;
    changes["width"] = width;
    box.width = width;
}

float Actor::get_height() const {
    return box.height;
}

void Actor::set_height(float height) {
    is_changed = true;
    changes["height"] = height;
    box.height = height;
}

float Actor::get_depth() const {
    return depth;
}

void Actor::set_depth(float depth) {
    is_changed = true;
    changes["depth"] = depth;
    this->depth = depth;
}

bool Actor::is_visible() const {
    return visible;
}

void Actor::set_visibility(bool visibility) {
    is_changed = true;
    changes["visible"] = visibility;
    visible = visibility;
}

bool Actor::get_use_caching() const {
    return use_caching;
}

void Actor::set_use_caching(bool use) {
    is_changed = true;
    changes["use-caching"] = use;
    use_caching = use;
}