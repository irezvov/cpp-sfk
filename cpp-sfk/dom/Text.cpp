//
//  Text.cpp
//  cpp-sfk
//
//  Created by Ilya Rezvov on 20.06.14.
//  Copyright (c) 2014 irezvov. All rights reserved.
//

#include "Text.h"

Text::ptr Text::create(const string &id) {
    return make_shared<Text>(id);
}

Text::Text(string const& id) : Actor("ClutterText", id), editable(true) {}

Json::Value Text::get_description() const {
    Json::Value description(Actor::get_description());
    description["color"] = get_color();
    description["text"] = get_text();
    if (!font_name.empty()) {
        description["font-name"] = get_font_name();
    }
    description["editable"] = is_editable();
    return move(description);
}

string const& Text::get_color() const {
    return color;
}

void Text::set_color(string const& color) {
    is_changed = true;
    changes["color"] = color;
    this->color = color;
}

string const& Text::get_text() const {
    return text;
}

void Text::set_text(string const& text) {
    is_changed = true;
    changes["text"] = text;
    this->text = text;
}

string const& Text::get_font_name() const {
    return font_name;
}

void Text::set_font_name(string const& font) {
    is_changed = true;
    changes["font-name"] = font;
    font_name = font;
}

bool Text::is_editable() const {
    return editable;
}

void Text::set_editable(bool editable) {
    is_changed = true;
    changes["editable"] = editable;
    this->editable = editable;
}