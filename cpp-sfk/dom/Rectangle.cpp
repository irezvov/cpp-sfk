//
//  Rectangle.cpp
//  cpp-sfk
//
//  Created by Ilya Rezvov on 19.06.14.
//  Copyright (c) 2014 irezvov. All rights reserved.
//

#include "Rectangle.h"

Rectangle::ptr Rectangle::create(const string &id) {
    return make_shared<Rectangle>(id);
}

Rectangle::Rectangle(string const& id): Actor("ClutterRectangle", id), color("black"), has_border(false),
    border_color("transparent"), border_width(0) { }

Json::Value Rectangle::get_description() const {
    Json::Value description(Actor::get_description());
    description["color"] = get_color();
    description["has-border"] = get_has_border();
    description["border-color"] = get_border_color();
    description["border-width"] = get_border_width();
    return move(description);
}

string const& Rectangle::get_color() const {
    return color;
}

void Rectangle::set_color(string const& color) {
    is_changed = true;
    changes["color"] = color;
    this->color = color;
}

bool Rectangle::get_has_border() const {
    return has_border;
}

void Rectangle::set_has_border(bool has) {
    is_changed = true;
    changes["has-border"] = has;
    has_border = has;
}

string const& Rectangle::get_border_color() const {
    return border_color;
}

void Rectangle::set_border_color(string const& color) {
    is_changed = true;
    changes["border-color"] = color;
    border_color = color;
}

uint32_t Rectangle::get_border_width() const {
    return border_width;
}

void Rectangle::set_border_width(uint32_t width) {
    is_changed = true;
    changes["border-width"] = width;
    border_width = width;
}