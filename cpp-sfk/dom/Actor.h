//
//  Actor.h
//  cpp-sfk
//
//  Created by Ilya Rezvov on 18.06.14.
//  Copyright (c) 2014 irezvov. All rights reserved.
//

#ifndef __cpp_sfk__Actor__
#define __cpp_sfk__Actor__

#include "common.h"
#include <json/json.h>

struct Placement {
    float x;
    float y;
    float width;
    float height;
};

class Actor {
public:
    typedef shared_ptr<Actor> ptr;
    Actor(string const& type, string const& id);
    virtual Json::Value const& get_changes() const;
    virtual void collect_changes(vector<Json::Value> &changes);
    virtual Json::Value get_description() const;
    bool has_changes() const;
    void commit_changes();
    
    string const& get_type() const;
    string const& get_id() const;
    float get_x() const;
    void set_x(float x);
    float get_y() const;
    void set_y(float y);
    float get_width() const;
    void set_width(float width);
    float get_height() const;
    void set_height(float height);
    float get_depth() const;
    void set_depth(float depth);
    bool is_visible() const;
    void set_visibility(bool visible);
    bool get_use_caching() const;
    void set_use_caching(bool use);
protected:
    bool is_changed;
    Json::Value changes;
private:
    string type;
    Placement box;
    string id;
    float depth;
    uint8_t opacity;
    bool visible;
    bool use_caching;
};

#endif /* defined(__cpp_sfk__Actor__) */
