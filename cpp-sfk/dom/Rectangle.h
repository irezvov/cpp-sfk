//
//  Rectangle.h
//  cpp-sfk
//
//  Created by Ilya Rezvov on 19.06.14.
//  Copyright (c) 2014 irezvov. All rights reserved.
//

#ifndef __cpp_sfk__Rectangle__
#define __cpp_sfk__Rectangle__


#include "common.h"
#include "Container.h"

class Rectangle: public Actor {
public:
    typedef shared_ptr<Rectangle> ptr;
    static ptr create(string const& id);
    Rectangle(string const& id);
    Json::Value get_description() const;
    
    string const& get_color() const;
    void set_color(string const& color);
    bool get_has_border() const;
    void set_has_border(bool has);
    string const& get_border_color() const;
    void set_border_color(string const& color);
    uint32_t get_border_width() const;
    void set_border_width(uint32_t color);
private:
    string color;
    bool has_border;
    string border_color;
    uint32_t border_width;
};

#endif /* defined(__cpp_sfk__Rectangle__) */
