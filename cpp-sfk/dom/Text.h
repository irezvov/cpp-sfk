//
//  Text.h
//  cpp-sfk
//
//  Created by Ilya Rezvov on 20.06.14.
//  Copyright (c) 2014 irezvov. All rights reserved.
//

#ifndef __cpp_sfk__Text__
#define __cpp_sfk__Text__

#include "common.h"
#include "Actor.h"

class Text : public Actor {
public:
    typedef shared_ptr<Text> ptr;
    static ptr create(string const& id);
    Text(string const& id);
    Json::Value get_description() const;
    
    string const& get_color() const;
    void set_color(string const& color);
    string const& get_text() const;
    void set_text(string const& text);
    string const& get_font_name() const;
    void set_font_name(string const& font);
    bool is_editable() const;
    void set_editable(bool editable);
private:
    string color;
    string text;
    string font_name;
    bool editable;
};

#endif /* defined(__cpp_sfk__Text__) */
